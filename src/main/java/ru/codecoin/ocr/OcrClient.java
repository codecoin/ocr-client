package ru.codecoin.ocr;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;

import ru.codecoin.ocr.model.OcrResponse;

/**
 * 
 */
public interface OcrClient extends Closeable, Serializable {

	/**
	 * 
	 * @param file
	 * @return
	 * @throws OcrException
	 * @throws IOException
	 */
	public OcrResponse recognize(File file) throws OcrException, IOException;

	/**
	 * 
	 * @param file
	 * @param lang
	 * @return
	 * @throws OcrException
	 * @throws IOException
	 */
	public OcrResponse recognize(File file, OcrLanguage lang) throws OcrException, IOException;

	/**
	 * 
	 * @param file
	 * @param lang
	 * @param requireOverlay
	 * @return
	 * @throws OcrException
	 * @throws IOException
	 */
	public OcrResponse recognize(File file, OcrLanguage lang, boolean requireOverlay) throws OcrException, IOException;

	/**
	 * 
	 * @param url
	 * @return
	 * @throws OcrException
	 * @throws IOException
	 */
	public OcrResponse recognize(URL url) throws OcrException, IOException;

	/**
	 * 
	 * @param url
	 * @param lang
	 * @return
	 * @throws OcrException
	 * @throws IOException
	 */
	public OcrResponse recognize(URL url, OcrLanguage lang) throws OcrException, IOException;

	/**
	 * 
	 * @param url
	 * @param lang
	 * @param requireOverlay
	 * @return
	 * @throws OcrException
	 * @throws IOException
	 */
	public OcrResponse recognize(URL url, OcrLanguage lang, boolean requireOverlay) throws OcrException, IOException;
}
