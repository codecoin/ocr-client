package ru.codecoin.ocr;

public class OcrException extends RuntimeException {

	public OcrException() {
	}

	public OcrException(String message) {
		super(message);
	}

	public OcrException(Throwable cause) {
		super(cause);
	}

	public OcrException(String message, Throwable cause) {
		super(message, cause);
	}

	public OcrException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	private static final long serialVersionUID = 4630748365292391798L;

}
