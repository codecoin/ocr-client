package ru.codecoin.ocr;

public enum OcrLanguage {

	CZECH("ce"), 
	DANISH("dan"), 
	DUTCH("dut"), 
	ENGLISH("eng"), 
	FINNISH("fin"), 
	FRENCH("fre"), 
	GERMAN("ger"), 
	HUNGARIAN("hun"), 
	ITALIAN("ita"), 
	NORWEGIAN("nor"), 
	POLISH("pol"), 
	PORTUGUESE("por"), 
	SPANISH("spa"), 
	SWEDISH("swe"), 
	CHINESE_SIMPLIFIED("chs"), 
	CHINESE_TRADITIONAL("cht"),
	GREEK("gre"), 
	JAPANESE("jpn"), 
	RUSSIAN("rus"), 
	TURKISH("tur"),  
	KOREAN("kor");
	
	private String code;

	private OcrLanguage(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
