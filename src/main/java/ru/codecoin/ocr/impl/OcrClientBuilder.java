package ru.codecoin.ocr.impl;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.Validate;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

import ru.codecoin.ocr.OcrClient;
import ru.codecoin.ocr.OcrException;

@NotThreadSafe
public class OcrClientBuilder {

	private static final String DEFAULT_API_URL = "http://api.ocr.space/parse/image";

	private String apiKey;
	private URL apiUrl;
	private HttpClientBuilder httpClientBuilder;

	public static OcrClientBuilder create() {
		return new OcrClientBuilder();
	}

	protected OcrClientBuilder() {
	}

	public OcrClientBuilder setApiKey(String apiKey) {
		this.apiKey = apiKey;
		return this;
	}

	public OcrClientBuilder setApiUrl(URL apiUrl) {
		this.apiUrl = apiUrl;
		return this;
	}

	public OcrClientBuilder setHttpClientBuilder(HttpClientBuilder httpClientBuilder) {
		this.httpClientBuilder = httpClientBuilder;
		return this;
	}

	public OcrClient build() {
		String finalApiKey = Validate.notBlank(this.apiKey);

		URL finalApiUrl = apiUrl;
		if (finalApiUrl == null) {
			try {
				finalApiUrl = new URL(DEFAULT_API_URL);
			} catch (MalformedURLException e) {
				throw new OcrException(e);
			}
		}

		CloseableHttpClient finalHttpClient = null;
		if (httpClientBuilder != null) {
			finalHttpClient = httpClientBuilder.build();
		} else {
			finalHttpClient = HttpClients.createMinimal();
		}

		return new OcrClientImpl(finalApiKey, finalApiUrl, finalHttpClient);
	}
}
