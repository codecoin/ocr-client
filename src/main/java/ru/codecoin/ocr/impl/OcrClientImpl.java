package ru.codecoin.ocr.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

import org.apache.commons.lang3.Validate;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;

import ru.codecoin.ocr.OcrClient;
import ru.codecoin.ocr.OcrException;
import ru.codecoin.ocr.OcrLanguage;
import ru.codecoin.ocr.model.OcrResponse;

@ThreadSafe
public class OcrClientImpl implements OcrClient {

	@Override
	public OcrResponse recognize(File file) throws OcrException, IOException {
		return doRecognize(file, null, OcrLanguage.ENGLISH, false);
	}

	@Override
	public OcrResponse recognize(File file, OcrLanguage lang) throws OcrException, IOException {
		return doRecognize(file, null, lang, false);
	}

	@Override
	public OcrResponse recognize(File file, OcrLanguage lang, boolean requireOverlay) throws OcrException, IOException {
		return doRecognize(file, null, lang, requireOverlay);
	}

	@Override
	public OcrResponse recognize(URL url) throws OcrException, IOException {
		return doRecognize(null, url, OcrLanguage.ENGLISH, false);
	}

	@Override
	public OcrResponse recognize(URL url, OcrLanguage lang) throws OcrException, IOException {
		return doRecognize(null, url, lang, false);
	}

	@Override
	public OcrResponse recognize(URL url, OcrLanguage lang, boolean requireOverlay) throws OcrException, IOException {
		return doRecognize(null, url, lang, requireOverlay);
	}

	@Override
	public void close() throws IOException {
		httpClient.close();
	}

	protected enum PostParameter {

		/**
		 * API key is available after registration on
		 * <a href="http://ocr.space/OCRAPI">ocr.space</a>.
		 */
		apikey,

		/**
		 * Multipart encoded image file with filename. The input image which
		 * will be analyzed by the OCR engine.
		 */
		file,

		/**
		 * URL of remote image file. Allows to specify a remote image URL
		 * instead of uploading the image file.
		 */
		url,

		/**
		 * Language code corresponding to the primary language the system should
		 * use to perform OCR. If no language is specified, English [eng] is
		 * taken as default.
		 * 
		 * @see OcrLanguage
		 */
		language,

		/**
		 * Allows to specify if the image/pdf text overlay is required. Overlay
		 * could be used to show the text over the image
		 */
		isOverlayRequired
	}

	protected OcrResponse doRecognize(File file, URL url, OcrLanguage lang, boolean requireOverlay) throws IOException {
		MultipartEntityBuilder requestBuilder = MultipartEntityBuilder.create();
		addTextBody(requestBuilder, PostParameter.apikey, apiKey, null);
		addTextBody(requestBuilder, PostParameter.language, lang.getCode(), OcrLanguage.ENGLISH.getCode());
		addTextBody(requestBuilder, PostParameter.isOverlayRequired, requireOverlay, Boolean.FALSE);
		addFileBody(requestBuilder, file, url);

		HttpPost httpPost = new HttpPost(apiUrl.toExternalForm());
		httpPost.setEntity(requestBuilder.build());

		return executeRequest(httpPost);
	}

	protected void addTextBody(MultipartEntityBuilder builder, PostParameter param, Object value, Object def) {
		if (!Objects.equals(value, def)) {
			builder.addTextBody(param.name(), value.toString());
		}
	}

	protected void addFileBody(MultipartEntityBuilder builder, File file, URL url) {
		Validate.validState(url != null || file != null && file.exists());

		File localFile = file;
		if (localFile == null && isLocalFile(url)) {
			try {
				file = new File(url.toURI());
			} catch (URISyntaxException e) {
				throw new OcrException("Unable to create file from URI: " + url.toExternalForm(), e);
			}
			Validate.validState(file.exists());
		}

		if (localFile != null) {
			builder.addBinaryBody(PostParameter.file.name(), localFile);
		} else {
			builder.addTextBody(PostParameter.url.name(), url.toExternalForm());
		}
	}

	protected OcrResponse executeRequest(HttpPost httpPost) throws IOException {
		try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return processResponse(response);
			}
			throw new OcrException("Unexpected OCR API http status code: " + response.getStatusLine().getStatusCode());
		}
	}

	protected OcrResponse processResponse(CloseableHttpResponse response) throws IOException {
		HttpEntity responseEntity = response.getEntity();
		Validate.validState(responseEntity != null, "OCR service suddenly didn't return recognition result");

		InputStream contentStream = responseEntity.getContent();
		try (BufferedReader contentReader = new BufferedReader(new InputStreamReader(contentStream))) {
			return OcrResponse.fromJson(contentReader);
		}
	}

	protected boolean isLocalFile(URL url) {
		if (url != null) {
			String protocol = url.getProtocol();
			return "file".equalsIgnoreCase(protocol);
		}
		return false;
	}

	protected OcrClientImpl(String apiKey, URL apiUrl, CloseableHttpClient httpClient) {
		assert apiKey != null;
		assert apiUrl != null;
		assert httpClient != null;

		this.apiKey = apiKey;
		this.apiUrl = apiUrl;
		this.httpClient = httpClient;
	}

	private final String apiKey;
	private final URL apiUrl;
	private final CloseableHttpClient httpClient;

	private static final long serialVersionUID = 7834990630850845191L;
}
