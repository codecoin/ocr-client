package ru.codecoin.ocr.impl;

import org.apache.http.annotation.Immutable;

import ru.codecoin.ocr.OcrClient;

@Immutable
public class OcrClients {

	private OcrClients() {
		super();
	}

	public static OcrClientBuilder custom() {
		return OcrClientBuilder.create();
	}

	public static OcrClient createDefault(String apiKey) {
		return OcrClientBuilder.create().setApiKey(apiKey).build();
	}
}
