package ru.codecoin.ocr.model;

public enum OcrExitCode {
	/**
	 * Parsed Successfully (Image / All pages parsed successfully)
	 */
	PARSED_SUCCESSFULLY(1),

	/**
	 * Parsed Partially (Only few pages out of all the pages parsed
	 * successfully)
	 */
	PARSED_PARTIALLY(2),

	/**
	 * Image / All the PDF pages failed parsing (This happens maily because
	 * the Parsing engine fails to parse an image)
	 */
	PARSING_FAILED(3),

	/**
	 * Error occurred when attempting to parse (This happens when a fatal
	 * error occurs during parsing image / PDF)
	 */
	PARSING_FATAL_ERROR(4);

	private int id;

	private OcrExitCode(int id) {
		this.id = id;
	}

	public static OcrExitCode get(int id) {
		if (id < 1 || id > 4) {
			throw new IndexOutOfBoundsException("Unknown OCR exit code " + id);
		}

		for (OcrExitCode exitCode : OcrExitCode.values()) {
			if (exitCode.id == id)
				return exitCode;
		}
		return null;
	}
}