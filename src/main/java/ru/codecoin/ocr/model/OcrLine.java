package ru.codecoin.ocr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class OcrLine implements Serializable, Iterable<OcrWord> {

	@XmlElement(name = "Words")
	protected List<OcrWord> words = new ArrayList<>();

	@XmlElement(name = "MaxHeight")
	protected Integer maxHeight;

	@XmlElement(name = "MinTop")
	protected Integer minTop;

	public List<OcrWord> getWords() {
		return Collections.unmodifiableList(words);
	}

	public Integer getMaxHeight() {
		return maxHeight;
	}

	public Integer getMinTop() {
		return minTop;
	}

	@Override
	public Iterator<OcrWord> iterator() {
		return getWords().iterator();
	}

	private static final long serialVersionUID = 204315154408368946L;
}