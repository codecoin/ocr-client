package ru.codecoin.ocr.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class OcrPage implements Serializable {

	@XmlElement(name = "TextOverlay")
	protected OcrTextOverlay textOverlay;

	@XmlElement(name = "FileParseExitCode")
	protected Integer exitCode;

	@XmlElement(name = "ParsedText")
	protected String text;

	@XmlElement(name = "ErrorMessage")
	protected String errorMessage;

	@XmlElement(name = "ErrorDetails")
	protected String errorDetails;

	public OcrTextOverlay getTextOverlay() {
		return textOverlay;
	}

	public OcrPageExitCode getExitCode() {
		return OcrPageExitCode.get(exitCode);
	}

	public String getText() {
		return text;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getErrorDetails() {
		return errorDetails;
	}

	private static final long serialVersionUID = 2367350559800798577L;
}