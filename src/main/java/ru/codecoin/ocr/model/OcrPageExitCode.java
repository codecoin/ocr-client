package ru.codecoin.ocr.model;

public enum OcrPageExitCode {
	FILE_NOT_FOUND(0), SUCCESS(1), ENGINE_ERROR(-10), TIMEOUT(-20), VALIDATION_ERROR(-30), UNKNOWN_ERROR(-99);

	private int id;

	private OcrPageExitCode(int id) {
		this.id = id;
	}

	public static OcrPageExitCode get(int id) {
		for (OcrPageExitCode exitCode : OcrPageExitCode.values()) {
			if (exitCode.id == id)
				return exitCode;
		}
		return null;
	}

}