package ru.codecoin.ocr.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;

import ru.codecoin.ocr.OcrException;

@XmlRootElement(name = "OcrResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class OcrResponse implements Serializable, Iterable<OcrPage> {

	@XmlElement(name = "ParsedResults")
	protected List<OcrPage> results = new ArrayList<>();

	@XmlElement(name = "OCRExitCode")
	protected Integer exitCode;

	@XmlElement(name = "IsErroredOnProcessing")
	protected Boolean errors;

	@XmlElement(name = "ErrorMessage")
	protected String errorMessage;

	@XmlElement(name = "ErrorDetails")
	protected String errorDetails;

	@XmlElement(name = "ProcessingTimeInMilliseconds")
	protected Long timeElapsed;

	/**
	 * An Array of all parsed results. The parsed results for image file / each
	 * page of PDF. Each has its own exit code, parsed result and error message
	 * (if any)
	 * 
	 * @return
	 */
	public List<OcrPage> getResults() {
		return Collections.unmodifiableList(results);
	}

	/**
	 * Primary exit code returned by the application for parsed results. The
	 * exit code signifies if the parsing of image / pdf completed successfully,
	 * partially or failed with error.
	 * 
	 * @return
	 * @see OcrExitCode
	 */
	public OcrExitCode getExitCode() {
		return OcrExitCode.get(exitCode);
	}

	/**
	 * If an error occurs when parsing the Image / PDF pages
	 * 
	 * @return
	 */
	public Boolean hasErrors() {
		return errors;
	}

	/**
	 * The error message of the error occurred when parsing the image
	 * 
	 * @return
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * The detailed error message
	 * 
	 * @return
	 */
	public String getErrorDetails() {
		return errorDetails;
	}

	public Long getTimeElapsed() {
		return timeElapsed;
	}

	@Override
	public Iterator<OcrPage> iterator() {
		return getResults().iterator();
	}

	public void toXml(Writer writer) {
		try {
			JAXBContext jc = JAXBContext.newInstance(OcrResponse.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/xml");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			marshal(marshaller, writer);
		} catch (JAXBException e) {
			throw new OcrException(e);
		}
	}

	public String toXmlString() {
		StringWriter output = new StringWriter();
		toXml(output);

		return output.toString();
	}

	public void toJson(Writer writer) {
		try {
			JAXBContext jc = JAXBContext.newInstance(OcrResponse.class);
			Marshaller marshaller = jc.createMarshaller();
			marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
			marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, false);
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			marshal(marshaller, writer);
		} catch (JAXBException e) {
			throw new OcrException(e);
		}
	}

	public String toJsonString() {
		StringWriter output = new StringWriter();
		toJson(output);

		return output.toString();
	}

	private void marshal(Marshaller marshaller, Writer writer) throws JAXBException {
		Writer bufferedWriter = writer instanceof BufferedWriter ? writer : new BufferedWriter(writer);
		marshaller.marshal(this, bufferedWriter);
	}

	public static OcrResponse fromXml(Reader reader) {
		try {
			JAXBContext jc = JAXBContext.newInstance(OcrResponse.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/xml");

			return unmarshal(unmarshaller, reader);
		} catch (JAXBException e) {
			throw new OcrException("Unable to parse OCR response", e);
		}
	}

	public static OcrResponse fromJson(Reader reader) {
		try {
			JAXBContext jc = JAXBContext.newInstance(OcrResponse.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
			unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, false);

			return unmarshal(unmarshaller, reader);
		} catch (JAXBException e) {
			throw new OcrException("Unable to parse OCR response", e);
		}
	}

	private static OcrResponse unmarshal(Unmarshaller unmarshaller, Reader reader) throws JAXBException {
		Reader bufferedReader = reader instanceof BufferedReader ? reader : new BufferedReader(reader);
		StreamSource source = new StreamSource(bufferedReader);
		return unmarshaller.unmarshal(source, OcrResponse.class).getValue();
	}

	private static final long serialVersionUID = 7842404713324619708L;
}