package ru.codecoin.ocr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class OcrTextOverlay implements Serializable, Iterable<OcrLine> {

	@XmlElement(name = "Lines")
	protected List<OcrLine> lines = new ArrayList<>();

	@XmlElement(name = "HasOverlay")
	protected Boolean overlays;

	@XmlElement(name = "Message")
	protected String message;

	/**
	 * An array of lines in the overlay text. This contains an array of all the
	 * lines. Each line will contain an array of words
	 * 
	 * @return
	 */
	public List<OcrLine> getLines() {
		return Collections.unmodifiableList(lines);
	}

	/**
	 * Overlay is present or not. True/False depending upon if the overlay for
	 * the parsed result is present or not
	 * 
	 * @return
	 */
	public Boolean hasOverlay() {
		return overlays;
	}

	/**
	 * Custom message from OCR API service
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	@Override
	public Iterator<OcrLine> iterator() {
		return getLines().iterator();
	}

	private static final long serialVersionUID = 1331865111514436395L;
}