package ru.codecoin.ocr.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class OcrWord implements Serializable {

	@XmlElement(name = "WordText")
	protected String text;

	@XmlElement(name = "Left")
	protected Integer left;

	@XmlElement(name = "Top")
	protected Integer top;

	@XmlElement(name = "Height")
	protected Integer height;

	@XmlElement(name = "Width")
	protected Integer width;

	/**
	 * Text of the word. This contains the text of that specific word
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * Distance of word from left (in Pixels). Contains the distance (in Pixels)
	 * of the word from the left edge of the original size of image
	 * 
	 * @return
	 */
	public Integer getLeft() {
		return left;
	}

	/**
	 * Distance of word from top (in Pixels). Contains the distance (in Pixels)
	 * of the word from the top edge of the original size of image
	 * 
	 * @return
	 */
	public Integer getTop() {
		return top;
	}

	/**
	 * Height of the word. Contains the height (in Pixels) of the word in the
	 * original size of image
	 * 
	 * @return
	 */
	public Integer getHeight() {
		return height;
	}

	/**
	 * Width of the word. Contains the width (in Pixels) of the word in the
	 * original size of image
	 * 
	 * @return
	 */
	public Integer getWidth() {
		return width;
	}

	private static final long serialVersionUID = -7402280938307902049L;
}