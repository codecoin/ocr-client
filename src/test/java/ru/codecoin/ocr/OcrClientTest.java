package ru.codecoin.ocr;

import java.io.File;
import java.io.IOException;

import ru.codecoin.ocr.impl.OcrClients;
import ru.codecoin.ocr.model.OcrExitCode;
import ru.codecoin.ocr.model.OcrPageExitCode;
import ru.codecoin.ocr.model.OcrResponse;

public class OcrClientTest {

	static File localFile = new File("path/to/local/file");

	public static void main(String[] args) throws IOException {
		try (OcrClient ocr = OcrClients.createDefault("xxx-yyy")) {
			OcrResponse result = ocr.recognize(localFile, OcrLanguage.RUSSIAN, false);

			// getting raw result in Json format...
			System.out.println(result.toJsonString());

			// or in xml...
			System.out.println(result.toXmlString());

			// process structured result
			if (result.getExitCode() == OcrExitCode.PARSED_SUCCESSFULLY || 
			    result.getExitCode() == OcrExitCode.PARSED_PARTIALLY) {

				// OcrResponse has collection of parsed pages
				result.forEach(page -> {
					if (page.getExitCode() == OcrPageExitCode.SUCCESS) {
						System.out.println("Parsed result: " + page.getText());

						if (page.getTextOverlay() != null) {
							page.getTextOverlay().forEach(line -> {

								System.out.println("Line min top:    " + line.getMinTop());
								System.out.println("Line max height: " + line.getMaxHeight());

								line.forEach(word -> {
									System.out.println("Word text:   " + word.getText());
									System.out.println("Word left:   " + word.getLeft());
									System.out.println("Word top:    " + word.getTop());
									System.out.println("Word witdh:  " + word.getWidth());
									System.out.println("Word height: " + word.getHeight());
								});
							});
						}

					} else {
						System.out.println("OCR Page processing status is " + page.getExitCode());
						System.out.println("Error message: " + page.getErrorMessage());
						System.out.println("Error details: " + page.getErrorDetails());
					}
				});

			} else {
				System.out.println("Error message: " + result.getErrorMessage());
				System.out.println("Error details: " + result.getErrorDetails());
			}
		}
	}

}
